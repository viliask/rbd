	<?php $db = NULL; ?>

    <footer><div class="container">
        <small><strong>Relacyjne bazy danych</strong>: projekt zaliczeniowy</small>
        <small>Paweł Wojnicz, Informatyka II 2015/2016</small>
    </div></footer>

    <!-- SCRIPTS -->
    <script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
    <script src="js/transition.min.js"></script>
    <script src="js/checkbox.min.js"></script>
    <script src="js/dropdown.min.js"></script>
    <script src="js/form.min.js"></script>
    <script src="js/main.min.js"></script>

</body>
</html>