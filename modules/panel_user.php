<h2>Dodaj pomiary:</h2>

<?php if(!$current_study = current_study()) { ?>
	<div class="alert info">Aktualnie nie jest prowadzone żadne badanie.</div>
<?php } else { ?>

	<form class="ui form" id="result_add" method="post" action="results_add.php">
        <div class="ui stacked segment">
            <div class="field">
                <label for="pow">Wybierz powierzchnię:</label>
                <select class="ui dropdown select" name="pole" required>
                	<option value="">Wybierz powierzchnię</option>
                    <?php foreach($pola = study_pola($current_study['idd']) as $pow): ?>
                	<option value="<?php echo $pow['id_pola']; ?>">powierzchnia "<?php echo $pow['powierzchnia']; ?>"</option>
                <?php endforeach; ?>
                </select>
            </div>
            <div class="field"> 
                <input type="hidden" name="person" value="<?php echo $person['ido']; ?>">
                <input class="ui blue fluid button" type="submit" name="submit" value="przejdź dalej" />
            </div>
        </div>
    </form>

<?php } ?>

<h2>Twoje pomiary:</h2>

<?php if (!$pomiary = person_results($person['ido'])) { ?>
    <div class="alert info">Brak dodanych pomiarów.</div>
<?php } else { ?>
<table>
    <thead><tr>
        <th>Doświadczenie</th>
        <th>Powierzchnia</th>
        <th>Obszar</th>
        <th>Wynik</th>
        <th>Data</th>
    </tr></thead>
    <tbody>
        <?php foreach ($pomiary as $p): ?>
        <tr>
            <td>Doświadczenie #<?php echo $p['idd']; ?></td>
            <td><?php echo $p['powierzchnia']; ?></td>
            <td><?php echo $p['wielkosc']; ?>m<sup>2</sup></td>
            <td><?php echo $p['wynik']; ?></td>
            <td><?php echo $p['data']; ?></td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>
<?php } ?>