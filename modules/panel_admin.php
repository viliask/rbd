<div id="resources">
    <nav class="two ui buttons" style="margin-bottom: 5em !important;">
       <a href="#rosliny" class="ui button">Zarządzaj roślinami</a>
       <a href="#nawozy" class="ui button">Zarządzaj nawozami</a>
    </nav>

    <article id="rosliny" class="ui segment">
        <form class="ui form" method="post" action="forms.php?form=roslina_add">
            <div class="field">
                <label>Dodaj nową roślinę</label>
                <input type="text" name="roslina" placeholder="Nazwa rośliny" required />
            </div>
            <input type="submit" value="Dodaj" class="ui button" />
        </form>
        <aside>
        <?php 
            if (!$rosliny = rosliny()) echo "Brak roślin w bazie danych";
            else {
                foreach ($rosliny as $r) $lista_roslin[] = $r['roslina'];
                echo implode(', ', $lista_roslin);
            }
        ?>
        </aside>
    </article>

    <article id="nawozy" class="ui segment">
        <form class="ui form" method="post" action="forms.php?form=nawoz_add">
            <div class="field">
                <label>Dodaj nowy nawóz</label>
                <input type="text" name="nawoz" placeholder="Nazwa nawozu" required />
            </div>
            <input type="submit" value="Dodaj" class="ui button" />
        </form>
        <aside>
        <?php 
            if (!$nawozy = nawozy()) echo "Brak nawozów w bazie danych";
            else {
                foreach ($nawozy as $n) $lista_nawozow[] = $n['nawoz'];
                echo implode(', ', $lista_nawozow);
            }
        ?>
        </aside>
    </article>
</div>

<section id="currentStudy">
    <h2>Trwające doświadczenie:</h2>

    <?php if(!$studyinfo = current_study()) { ?>
        <div class="alert info"><p>Aktualnie nie jest prowadzone żadne badanie.</p> <a href="study_add.php" class="ui button">Kliknij tutaj, aby zainicjować nowe</a></div>
    <?php } else require_once('modules/study_info.php'); ?>
</section>

<section id="finStudies">
    <h2>Doświadczenia zakończone:</h2>
        
    <?php if(!$fin_studies = fin_studies()) { ?>
        <div class="alert info">Aktualnie ma istnieją żadne ukończone badania.</div>
    <?php } else { ?>
               
        <table>
            <thead><tr>
                <th>ID</th>
                <th>Start</th>
                <th>Stop</th>
            </tr></thead>
            <tbody class="links">
                <?php foreach ($fin_studies as $fin): ?>
                <tr onclick="window.document.location='study_info.php?id=<?php echo $fin['idd']; ?>';">
                    <td>#<?php echo $fin['idd']; ?></td>
                    <td><?php echo $fin['start']; ?></td>
                    <td><?php echo $fin['stop']; ?></td>
                </tr>
                <?php endforeach; ?>
            </tbody>
            <tfoot>
                <tr>
                    <td colspan="7" class="note">* kliknij w rekord, aby zobaczyć pomiary</td>
                </tr>
            </tfoot>
        </table>

    <?php } ?>
</section>