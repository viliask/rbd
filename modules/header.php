<!DOCTYPE html>
<html lang="pl">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=Edge, chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><?php echo $title; ?> - interfejs administracyjny</title>
    <meta name="description" content="">
    <meta name="keywords" content="">

    <link rel="stylesheet" href="css/styles.min.css">
</head>
<body>
	<?php
		if (isset($_GET['user'])) {
			$person = $_GET['user'];
		    if (!$person = person($person)) $er404 = true;
		}
	?>

	<header>
        <div class="container">
            <a href="index.php"><h1>Interfejs administracyjny</h1></a>
            <?php if (isset($person)) { ?><span>Zalogowany jako <span><?php echo $person['stanowisko'] . ' ' . $person['osoba']; ?></span></span><?php } ?>
        </div>
    </header>

    <?php
    	// wyświetl błąd jeśli nie ma takiego id w DB
    	if (isset($er404)) { ?>
    		
			<main class="container">
				<div class="alert error">Błąd 404: nie ma takiej strony!</div>
			</main>

    		<?php require_once "modules/footer.php";
    		die();
    	}
    ?>