<div class="ui stacked segment" id="study_info">
    <header>
        <div style="float: left;">
            <h3>Doświadczenie #<?php echo $studyinfo['idd']; ?></h3>
            <span style="display: block;">Data rozpoczęcia: <?php echo $studyinfo['start']; ?></span>
            <?php if ($studyinfo['stop']) { ?><span>Data zakończenia: <?php echo $studyinfo['stop']; ?></span><?php } ?>
        </div>
        <?php if (!$studyinfo['stop']) { require_once('forms/study_end.php'); } ?>
    </header>
    <?php 
        foreach (study_pola ($studyinfo['idd']) as $pole):
    ?>
    <article>
        <aside>
            <h4>Powierzchnia "<?php echo $pole['powierzchnia']; ?>"</h4>
            <ul>
                <?php
                    $nawozy = study_nawozy($pole['id_pola']);
                    foreach($nawozy as $nawoz):
                ?>
                <li><?php echo $nawoz['nawoz']; ?></li>
                <?php endforeach; ?>
            </ul>
            <table>
                <thead><tr>
                    <th>Wielkość</th>
                    <th>Roślina</th>
                </tr></thead>
                <tbody>
                    <?php 
                        $obszary = study_obszary($pole['id_pola']);
                        foreach($obszary as $obszar):

                    ?>
                    <tr>
                        <td><?php echo $obszar['wielkosc']; ?> m<sup>2</sup></td>
                        <td><?php echo $obszar['roslina']; ?></td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </aside>
        <div class="pomiary">
            <h5>Pomiary</h5>
            <?php if (!$pomiary = study_pomiary($pole['id_pola'])) { ?>
                <div class="alert info">Brak pomiarów.</div>
            <?php } else { ?>
            <table>
                <thead><tr>
                    <th>Obszar</th>
                    <th>Wynik</th>
                    <th>Data</th>
                    <th>Laborant</th>
                </tr></thead>
                <tbody>
                    <?php foreach ($pomiary as $pomiar): ?>
                    <tr>
                        <td><?php echo $pomiar['wielkosc']; ?><sup>2</sup></td>
                        <td><?php echo $pomiar['wynik']; ?></td>
                        <td><?php echo $pomiar['data']; ?></td>
                        <td><?php echo $pomiar['osoba']; ?></td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
            <?php } ?>
        </div>
    </article>
    <?php endforeach; ?>
</div>