<?php
    $title = "Informacje o doświadczeniu";
    require_once('inc/functions.php');
    require_once('modules/header.php');
    $study_id = $_GET['id'];    
?>

    <main class="container">

    <?php 
        if(!$studyinfo = study_info($study_id)) 
            alert("404, podana strona nie istnieje :(", "error");
    ?>

        <h2>Szczegółowe informacje: doświadczenie #<?php echo $studyinfo['idd']; ?></h2>
                   
        <?php require_once('modules/study_info.php'); ?>

    </main>

<?php require_once('modules/footer.php'); ?>