<?php
	$title = "Rejestracja";
    require_once('inc/functions.php');
 	require_once('modules/header.php');
?>

    <main class="container">
        <h2>Dodaj nowe konto:</h2>

        <form class="ui form" method="post" action="forms.php?form=signup">
            <div class="ui segment">
                <div class="field">
                    <label for="user_name">Imię i nazwisko:</label>
                    <input type="text" name="user_name" id="user_name" required />
                </div>
                <div class="field">
                    <label for="user_post">Stanowisko:</label>
                    <?php
                        if(!$posts = posts()) {
                            echo "Błąd! Brak stanowisk do wyświetlenia.";
                        } else {
                    ?>
                    <select class="ui dropdown select" name="user_post" id="user_post">
                        <option value="">Wybierz stanowisko</option>
                        <?php foreach($posts as $post): ?>
                        <option value="<?php echo $post['ids']; ?>"><?php echo $post['stanowisko']; ?></option>
                        <?php endforeach; ?>
                    </select>
                    <?php } ?>
                </div>
                <div class="field">
                    <input class="ui blue fluid button" type="submit" name="submit" value="stwórz konto" />
                </div>
            </div>
        </form>
    </main>

<?php require_once('modules/footer.php'); ?>