<?php
    $title = "Panel";
    require_once('inc/functions.php');
    require_once('modules/header.php');
?>

    <main class="container"><?php 

        if(ifNazdorca($person['stanowisko'])) 
            require_once("modules/panel_admin.php");
        else 
            require_once("modules/panel_user.php");
        
    ?></main>

<?php require_once('modules/footer.php'); ?>