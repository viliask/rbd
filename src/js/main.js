// semantic ui dropdown
$('.ui.dropdown').dropdown();

// zakończ doświadczenie - button
$('#currentStudy input[name="study_end"]').click(function(e) {

	e.preventDefault();
	var $form = $('#currentStudy form');

	if (confirm('Czy na pewno chcesz zakończyć bieżące doświadczenie?')) {

		// msg
		var success = '<div class="alert success"><p>Badanie zostało zakończone pomyślnie!</p> <a href="study_add.php" class="ui red button">Dodaj nowe</a></div>';
		var error = '<div class="alert error"><p>Wystąpił nieoczekiwany błąd :(</p></div>';

		$.post($form.attr('action'), $form.serialize(), function(data) {
            if (data === 'nok') { // nie działa :(
            	$('#study_info').slideUp('fast', function() {
					$('#currentStudy').append(error);
				});
			} else {
				$('#study_info').slideUp('fast', function() {
					$('#currentStudy').append(success);
				});
			}
		});
	}
});

// pokaż rośliny
$('a[href="#rosliny"]').click(function () {
	$('article#rosliny').slideToggle();
	return false;
});

$('a[href="#nawozy"]').click(function () {
	$('article#nawozy').slideToggle();
	return false;
});

/* ---------------
	formularze
--------------- */

// powierzchnia: wybierz powierzchnie
$('#study_add .segments :checkbox').change(function() {

	var content = $(this).parentsUntil('.segments').next('.secondary');

	if($(this).is(':checked')) {
		content.slideDown().prop('disabled', false);
	} else {
		content.slideUp().prop('disabled', true);
	}

});

// powierzchnia: pokaż ukryj .usunObszar
function toggle_rmButton($clones) {
	var count = $clones.children('.clone').size();
	if (count > 2) {
		$('.rmDiv').fadeIn();
	} else {
		$('.rmDiv').fadeOut();
	}
}

// powierzchnia: dodaj obszar
$('.cloneDiv').click(function() {

	var clones = $(this).parent().children('.clones');
	clones.children('.clone:first').clone().appendTo(clones).hide().slideDown();
	$('.ui.dropdown').dropdown(); // semantic ui function
	toggle_rmButton(clones);
});

// powierzchnia: usuń ostatni obszar
$('.rmDiv').click(function() {
	var clones = $(this).parent().children('.clones');
	clones.children('.clone:last').slideUp('fast', function() { $(this).remove(); });
	toggle_rmButton(clones);
});

// dodaj wynik pomiaru
$('#result_add select[name="poww"]').change(function() {
	$('form').submit();
});