<?php

if ($_SERVER['REQUEST_METHOD'] == 'POST') {

	$ido = $_POST['ido'];
	$id_obszaru = array();
	$id_obszaru = $_POST['id_obszaru'];
	$wynik = array();
	$wynik = $_POST['wynik'];

	// walidacja danych:

	$error = 0;
	$error_text = array();

	if (!isset($ido) || empty($ido) || !is_numeric($ido) || $ido <= 0 ) {
		$error++;
		$error_text[] = "Błędne id laboranta";
	}

	foreach ($id_obszaru as $obszar) {
		if (!isset($obszar) || empty($obszar) || !is_numeric($obszar) || $obszar <= 0) {
			$error++;
			$error_text[] = "Błędne id obszaru";
		}
	}
	

	foreach ($wynik as $w) {
		if (!isset($w) || empty($w)) {
			$error++;
			$error_text[] = "Wynik musi zostać uzupełniony.";
		}

		if ($w < 0) {
			$error++;
			$error_text[] = "Wynik musi być większy od 0.";
		}

		if (!is_numeric($w)) {
			$error++;
			$error_text[] = "Wynik musi być liczbą.";
		}
	}

	if ($error) alert($error_text, "error");

	// dodaj rekordy:

	try {
		require_once "inc/db.php";
		$db->beginTransaction();

		$sql = $db->prepare
		(
			'INSERT INTO Pomiary (id_obszaru, ido, data, wynik)
			VALUES(:id_obszaru, :ido, NOW(), :wynik)'
		);
		
		$sql->bindValue(':ido', $ido, PDO::PARAM_INT);

		foreach(array_combine($wynik, $id_obszaru) as $w => $i) {
			$sql->bindValue(':id_obszaru', $i, PDO::PARAM_INT);
			$sql->bindValue(':wynik', $w, PDO::PARAM_INT);
			$sql->execute();
		}

		$db->commit();
		alert("Wyniki dodane pomyślnie.", "success");
	}
	
	catch (PDOException $e) {
		$db->rollback();
		alert("Wystąpił nieoczkiwany błąd bazy danych, powiadom administratora.", "error");
	}
	
	$db = null;
}

?>
