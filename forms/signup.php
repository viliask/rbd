<?php

if ($_SERVER['REQUEST_METHOD'] == 'POST') {

	$uPost = $_POST['user_post'];
	$uName = test_input($_POST['user_name']);

	// walidacja danych:

	$error = 0;
	$error_text = array();

	if (!is_numeric($uPost)) {
		$error++;
		$error_text[] = "Nie kombinuj ;)";
	}

	if (!isset($uPost) || empty($uPost) || !isset($uName) || empty($uName)) {
		$error++;
		$error_text[] = "Należy uzupełnić wszystkie pola.";
	}

	if ($error) alert($error_text, "error");

	// dodaj rekordy:

	try {
		require_once('inc/db.php');

	    $sql = $db->prepare("INSERT INTO Osoby (ids, osoba) VALUES (:ids, :osoba)");
	    $sql->bindValue(':ids', $uPost, PDO::PARAM_INT);
	    $sql->bindValue(':osoba', $uName , PDO::PARAM_STR);

	    $sql->execute();
	    alert("Twoje konto zostało założone.", "success");
	} 

	catch (PDOException $e) {
		$db->rollback();
        alert("Wystąpił nieoczkiwany błąd bazy danych, powiadom administratora.", "error");
	}

    $db = null;
}

?>