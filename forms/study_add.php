<?php

if ($_SERVER['REQUEST_METHOD'] == 'POST') {

	$powierzchnia = array();
	@$powierzchnia = $_POST['powierzchnia'];

	$nawoz = array();
	@$nawoz = $_POST['nawozy'];

	$obszar = array();
	@$obszar = $_POST['obszar'];

	$roslina = array();
	@$roslina = $_POST['roslina'];

	// walidacja danych:

	$error = 0;
	$error_text = array();

	if (!isset($powierzchnia) || empty($powierzchnia)) {
		$error++;
		$error_text[] = "Nie wybrano żadnej powierzchni";
	}

	if (!isset($nawoz)) {
		$error++;
		$error_text[] = "Nie wybrano nawozów.";

	} else {
		foreach ($nawoz as $key) foreach ($key as $n) {
			if (!isset($n) || empty($n)) {
				$error++;
				$error_text[] = "Nie wybrano nawozów.";
				break;
			}
		}
	}
	
	if (!isset($obszar)) {
		$error++;
		$error_text[] = "Nie ustalono wielkości obszaru lub ustalono błędny.";

	} else {
		foreach ($obszar as $key) foreach ($key as $o) {
			if (!isset($o) || empty($o) || $o <= 0) {
				$error++;
				$error_text[] = "Nie ustalono wielkości obszaru lub ustalono błędny.";
				break;
			}
		}
	}

	if (!isset($roslina)) {
		$error++;
		$error_text[] = "Nie wybrano rośliny.";

	} else {
		foreach ($roslina as $key) foreach ($key as $r) {
			if (!isset($r) || empty($r)) {
				$error++;
				$error_text[] = "Nie wybrano rośliny.";
				break;
			}
		}
	}

	if ($error) alert($error_text, "error");

	// dodaj rekordy:

	try {
		require_once "inc/db.php";
		$db->beginTransaction();

		// dodaj doświadczenie
		$db->exec('INSERT INTO Doswiadczenia (start) VALUES(NOW())');
		$idd = $db->lastInsertId();	

		foreach ($powierzchnia as $pow) {
			// dodaj pole
			$sql_pole = $db->prepare('INSERT INTO Pola (id_pow, idd) VALUES(:id_pow, :idd)');
			$sql_pole->bindValue(':idd', $idd, PDO::PARAM_INT);
			$sql_pole->bindValue(':id_pow', $pow, PDO::PARAM_INT);
			$sql_pole->execute();
			$id_pola = $db->lastInsertId();

			// przypisz nawozy do pola
			$sql_nawozy = $db->prepare('INSERT INTO Pola_nawozy (id_pola, idn) VALUES(:id_pola, :idn)');
			$sql_nawozy->bindValue(':id_pola', $id_pola, PDO::PARAM_INT);
			foreach($nawoz['pow'.$pow] as $n) {
				$sql_nawozy->bindValue(':idn', $n, PDO::PARAM_INT); // !!! ARRAY
				$sql_nawozy->execute();
			}
			
			// przypisz obszary do pola
			$sql_obszary = $db->prepare('INSERT INTO Obszary (id_pola, wielkosc, idr) VALUES(:id_pola, :wielkosc, :idr)');
			$sql_obszary->bindValue(':id_pola', $id_pola, PDO::PARAM_INT);
			foreach(array_combine($obszar['pow'.$pow], $roslina['pow'.$pow]) as $w => $r) {
				$sql_obszary->bindValue(':wielkosc', $w, PDO::PARAM_INT); // !!! ARRAY
				$sql_obszary->bindValue(':idr', $r, PDO::PARAM_INT); // !!! ARRAY
				$sql_obszary->execute();
			}
		}

		$db->commit();
		alert("Doświadczenie zostało dodane pomyślnie.", "success");
	}
	
	catch (PDOException $e) {
		$db->rollback();
		alert("Wystąpił nieoczkiwany błąd bazy danych, spróbuj ponownie.", "error");
	}
	
	$db = null;
}

?>
