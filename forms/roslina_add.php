<?php

if ($_SERVER['REQUEST_METHOD'] == 'POST') {

	$roslina = test_input($_POST['roslina']);
	$roslina = strtolower($roslina);

	// walidacja danych:

	$error = 0;
	$error_text = array();

	if (!isset($roslina) || empty($roslina)) {
		$error++;
		$error_text[] = "Należy podać nazwę.";
	}

	if (is_numeric($roslina)) {
		$error++;
		$error_text[] = "Nazwa nie może być liczbą.";
	}

	$rosliny = rosliny();
	foreach ($rosliny as $r) {
		if (strcasecmp($roslina, $r['roslina']) == 0) {
			$error++;
			$error_text[] = "Taka roślina już istnieje w bazie danych.";
			break;
		}
	}

	if ($error) alert($error_text, "error");

	// dodaj rekordy:

	try {
		require_once('inc/db.php');

	    $sql = $db->prepare("INSERT INTO Rosliny (roslina) VALUES (:roslina)");
	    $sql->bindValue(':roslina', $roslina, PDO::PARAM_STR);

	    $sql->execute();
	    alert("Nowa roślina została dodana.", "success");
	} 

	catch (PDOException $e) {
        alert("Wystąpił nieoczkiwany błąd bazy danych, spróbuj ponownie.", "error");
    }

    $db = null;
}

?>