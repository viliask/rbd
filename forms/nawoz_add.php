<?php

if ($_SERVER['REQUEST_METHOD'] == 'POST') {

	$nawoz = test_input($_POST['nawoz']);
	$nawoz = strtolower($nawoz);

	// walidacja danych:

	$error = 0;
	$error_text = array();

	if (!isset($nawoz) || empty($nawoz)) {
		$error++;
		$error_text[] = "Należy podać nazwę.";
	}

	if (is_numeric($nawoz)) {
		$error++;
		$error_text[] = "Nazwa nie może być liczbą.";
	}

	$nawozy = nawozy();
	foreach ($nawozy as $n) {
		if (strcasecmp($nawoz, $n['nawoz']) == 0) {
			$error++;
			$error_text[] = "Taki nawóz już istnieje w bazie danych.";
			break;
		}
	}

	if ($error) alert($error_text, "error");

	// dodaj rekordy:

	try {
		require_once('inc/db.php');

	    $sql = $db->prepare("INSERT INTO Nawozy (nawoz) VALUES (:nawoz)");
	    $sql->bindValue(':nawoz', $nawoz, PDO::PARAM_STR);

	    $sql->execute();
	    alert("Nowy nawóz został dodany.", "success");
	} 

	catch (PDOException $e) {
        alert("Wystąpił nieoczkiwany błąd bazy danych, powiadom administratora.", "error");
    }

    $db = null;
}

