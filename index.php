<?php
	$title = "Strona główna";
    require_once('inc/functions.php');
 	require_once('modules/header.php');
?>

    <main class="container centred_form">
        <h2>Zaloguj się jako:</h2>

        <?php if($persons = persons()) { ?>
        <ul>
            <?php foreach ($persons as $person): ?>
            <li<?php if(ifNazdorca($person['stanowisko'])) { ?> class="admin"<?php } ?>>
                <a href="panel.php?user=<?php echo $person['ido']; ?>"><span><?php echo $person['stanowisko']; ?></span> <?php echo $person['osoba']; ?></a>
            </li>
        <?php endforeach; ?>
        </ul>

        <?php } else { ?>
            <div class="alert info">Brak użytkowników w bazie danych.</div>
        <?php } ?>

        <aside>
            <a href="signup.php">Dodaj nowe konto</a>
        </aside>
    </main>

<?php require_once('modules/footer.php'); ?>