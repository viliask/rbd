Baza - obsługa doświadczenia - opis modelu
Nadzorca:

Doświadczenie polega na zasiewaniu roślin i badaniu wpływu nawozów na ich wzrost. Doświadczenie prowadzi się na wielu powierzchniach. Na całej  powierzchni stosowana jest dowolna liczba nawozów (bez uwzględnienia ilości - czyli albo jest nawóz albo go nie ma). Powierzchnie  są podzielone na obszary (podpowierzchnie), cechą obszaru jest jego rozmiar (pole powierzchni). Na danym obszarze w jednym czasie jest tylko jedna roślina.

Laborant:

Laborant pobiera próbki liczbowe z całej powierzchni. Wynikiem doświadczenia jest liczba (może to być np liczba roślin które urosły, średnia wielkość rośliny itp) oraz data pomiaru. Laborantów jest wielu. Laborant może (nie musi) pomierzyć wiele powierzchni. W czasie trwania doświadczenia laborant może pomierzyć wiele razy tą samą powierzchnię.

Baza ma przechowywać historię doświadczeń i wyników. Doświadczenie kiedyś się kończy, i następuje nowe- to znaczy nowe rośliny, nowe nawozy. Dla ułatwienia można przyjąć, że podział powierzchni na obszary jest niezmienny. 

Interface
Baza ma być obsługiwana przez WWW. Technologia dowolna (może być PHP). Nie interesuje mnie autoryzacja i grafika projektu.

Muszą być oddzielone funkcjonalności Nadzorcy (admin) od Laboranta. Nadzorca może inicjować nowe doświadczenie i odczytywać zebrane wyniki.

Laborant zbiera wyniki i przez formularz dopisuje do bazy. Baza identyfikuje doświadczenie, laboranta, wynik, termin, roślinę, nawozy.

--------------------------

Czyli aktualne jest tylko 1 doświadczenie naraz - to wiele zmienia :) Zatem nadzorca inicjując doświadczenie:
- wybiera powierzchnie (kilka)
- ustala nawozy na danych powierzchniach
- ustala rozmiary podpowierzchni i przypisuje im roślinę

A laborant dodając pomiar:
- dopisuje wynik do danej powierzchni

W takim razie baza danych powinna prezentować się następująco:

STANOWISKO
- ids
- stanowisko

OSOBA
- ido
- osoba
- ids (rel. do stanowiska)

ROŚLINY
- idr
- roślina

NAWOZY
- idn
- nawóz

DOŚWIADCZENIE
- idd
- start
- stop

POWIERZCHNIE
- id_pow
- powierzchnia

DOŚWIADCZENIE_POLA
- id_pola
- id_pow (rel. do powierzchni)
- idd (rel. do doświadczenia)

POLA_NAWOZY
- id_pola (rel. do doświadczenie_pola)
- idn (rel. do nawozy)

OBSZARY
- id_pola (rel. do doświadczenie_pola)
- wielkosc
- roslina

POMIAR
- id_pola (rel. do doświadczenie_pola)
- wynik
- data
- ido (rel. do osoby)

Czy dobrze zrozumiałem? Prosiłbym bardzo o weryfikację.