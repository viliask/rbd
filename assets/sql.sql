CREATE TABLE Stanowiska (
	ids int NOT NULL AUTO_INCREMENT PRIMARY KEY,
	stanowisko varchar(20) NOT NULL
);

CREATE TABLE Osoby (
	ido int NOT NULL AUTO_INCREMENT PRIMARY KEY,
	ids int,
	osoba varchar(50) NOT NULL
);

CREATE TABLE Rosliny (
	idr int NOT NULL AUTO_INCREMENT PRIMARY KEY,
	roslina varchar(50) NOT NULL
);

CREATE TABLE Nawozy (
	idn int NOT NULL AUTO_INCREMENT PRIMARY KEY,
	nawoz varchar(40) NOT NULL
);

CREATE TABLE Doswiadczenia (
	idd int NOT NULL AUTO_INCREMENT PRIMARY KEY,
	start date NOT NULL,
	stop date
);

CREATE TABLE Powierzchnie (
	id_pow int NOT NULL AUTO_INCREMENT PRIMARY KEY,
	powierzchnia varchar(15) NOT NULL
);

CREATE TABLE Pola (
	id_pola int NOT NULL AUTO_INCREMENT PRIMARY KEY,
	idp int NOT NULL,
	idd int NOT NULL
);

CREATE TABLE Pola_nawozy (
	id_pola_nawozy int NOT NULL AUTO_INCREMENT PRIMARY KEY,
	id_pola int NOT NULL,
	idn int NOT NULL
);

CREATE TABLE Obszary (
	id_obszaru int NOT NULL AUTO_INCREMENT PRIMARY KEY,
	id_pola int NOT NULL,
	wielkosc int NOT NULL,
	idr int NOT NULL
);

CREATE TABLE Pomiary (
	id_pomiaru int NOT NULL AUTO_INCREMENT PRIMARY KEY,
	id_obszaru int NOT NULL,
	ido int NOT NULL,
	data date NOT NULL,
	wynik int
);

-- Stanowiska
INSERT INTO Stanowiska (stanowisko) VALUES('nazdorca');
INSERT INTO Stanowiska (stanowisko) VALUES('laborant');

-- Osoby
INSERT INTO Osoby (ids,osoba) VALUES(1,'David Gilmour');
INSERT INTO Osoby (ids,osoba) VALUES(2,'Syd Barrett');
INSERT INTO Osoby (ids,osoba) VALUES(2,'Roger Waters');
INSERT INTO Osoby (ids,osoba) VALUES(2,'Richard Wright');
INSERT INTO Osoby (ids,osoba) VALUES(2,'Nick Mason');

-- ROŚLINY
INSERT INTO Rosliny (roslina) VALUES('trawa');
INSERT INTO Rosliny (roslina) VALUES('czterolistna koniczynka');
INSERT INTO Rosliny (roslina) VALUES('ayahuasca');
INSERT INTO Rosliny (roslina) VALUES('miodokrzew');
INSERT INTO Rosliny (roslina) VALUES('rooibos');
INSERT INTO Rosliny (roslina) VALUES('camellia sinesis');
INSERT INTO Rosliny (roslina) VALUES('ostrokrzew paragwajski');

-- Nawozy
INSERT INTO Nawozy (nawoz) VALUES('mocny');
INSERT INTO Nawozy (nawoz) VALUES('średni');
INSERT INTO Nawozy (nawoz) VALUES('słaby');
INSERT INTO Nawozy (nawoz) VALUES('bardzo słaby');

-- Powierzchnie
INSERT INTO Powierzchnie (powierzchnia) VALUES('wschód');
INSERT INTO Powierzchnie (powierzchnia) VALUES('zachód');
INSERT INTO Powierzchnie (powierzchnia) VALUES('południe');
