<?php
	$title = "Formularz";
    require_once('inc/functions.php');
 	require_once('modules/header.php');
?>

    <main class="container">
        
        <?php
            if (isset($_GET['form']))
                $form = $_GET['form'];

            switch ($form) {

                case 'study_add':
                    require_once "forms/study_add.php";
                    break;
                case 'results_add':
                    require_once "forms/results_add.php";
                    break;
                case 'signup':
                    require_once "forms/signup.php";
                    break;
                case 'roslina_add':
                    require_once "forms/roslina_add.php";
                    break;
                case 'nawoz_add':
                    require_once "forms/nawoz_add.php";
                    break;
            }
        ?>

    </main>

<?php require_once('modules/footer.php'); ?>