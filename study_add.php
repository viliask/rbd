<?php
    $title = "Dodaj doświadczenie";
    require_once('inc/functions.php');
    require_once('modules/header.php'); 
?>

	<main class="container">
		<h2>Dodaj nowe doświadczenie:</h2>
        <?php 

            $powierzchnie = powierzchnie();
            $nawozy = nawozy();
            $rosliny = rosliny();

            // taka tam obsługa braków w bazie
            if (!$powierzchnie) { ?>
                <div class="alert error">Błąd! Brak powierzchni w bazie danych!</div>
            <?php } else if (!$nawozy) { ?>
                <div class="alert error">Błąd! Brak nawozów w bazie danych!</div>
            <?php } else if (!$rosliny) { ?>
                <div class="alert error">Błąd! Brak roślin w bazie danych!</div>
            <?php } else {
        ?>
		<form class="ui form" id="study_add" method="post" action="forms.php?form=study_add">
            <div class="field">
                <label>Start:</label>
                <?php echo date('d-m-Y'); ?>
            </div>

            <?php foreach($powierzchnie as $pow): ?>
            <div class="ui segments powierzchnia">
                <div class="ui segment">
                    <div class="ui toggle checkbox">
                        <input type="checkbox" name="powierzchnia[]" value="<?php echo $pow['id_pow']; ?>">
                        <label>Powierzchnia "<?php echo $pow['powierzchnia']; ?>"</label>
                    </div>
                </div>

                <fieldset class="ui secondary segment" disabled>
                    <div class="field">
                        <label>Wybierz nawozy</label>
                        <select name="nawozy[pow<?php echo $pow['id_pow']; ?>][]" class="ui selection dropdown" multiple>
                            <option value="">Wybierz nawozy</option>
                            <?php foreach($nawozy as $nawoz): ?>
                            <option value="<?php echo $nawoz['idn']; ?>"><?php echo $nawoz['nawoz']; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>

                    <fieldset class="clones">
                        <div class="two fields clone">
                            <div class="field">
                                <label>Wielkość obszaru:</label>
                                <input type="number" min="1" name="obszar[pow<?php echo $pow['id_pow']; ?>][]" placeholder="Wielkość obszaru">
                            </div>
                            <div class="field">
                                <label>Roślina:</label>
                                <div class="ui selection dropdown">
                                    <input type="hidden" name="roslina[pow<?php echo $pow['id_pow']; ?>][]">
                                    <div class="default text">Wybierz roślinę</div>
                                    <div class="menu">
                                        <?php foreach($rosliny as $roslina): ?>
                                        <div class="item" data-value="<?php echo $roslina['idr']; ?>"><?php echo $roslina['roslina']; ?></div>
                                        <?php endforeach; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </fieldset>

                    <button class="ui button cloneDiv" type="button">Dodaj obszar</button>
                    <button class="ui basic button rmDiv" type="button" style="display: none;">Usuń ostatni obszar</button>
                </fieldset>
            </div>
            <?php endforeach; ?>

            <input class="ui blue fluid button" type="submit" name="submit" value="dodaj doświadczenie" />
        </form>
        <?php } ?>
	</main>

<?php
    require_once('modules/footer.php'); 
?>