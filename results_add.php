<?php
	$title = "Dodaj pomiar";
	require_once('inc/functions.php');
	require_once('modules/header.php');

	if ($_SERVER['REQUEST_METHOD'] == 'POST') {
		$pole = $_POST['pole'];
		$person = person($_POST['person']);
?>

<main class="container">
	
	<h2>Dodaj pomiar dla pola "<?php echo $pole; ?>"</h2>

	<div class="ui stacked segment">
		<form class="ui form" method="post" action="forms.php?form=results_add">
			<?php foreach($obszary = study_obszary($pole) as $obszar): ?>
		    <div class="field">
		        <label for="wynik">Wynik dla obszaru <?php echo $obszar['wielkosc']; ?> m<sup>2</sup></label>
		        <input type="number" name="wynik[]" id="wynik" />
		        <input type="hidden" name="id_obszaru[]" value="<?php echo $obszar['id_obszaru']; ?>">
		    </div>
		    <?php endforeach; ?>
		    <div class="field"> 
                <input type="hidden" name="ido" value="<?php echo $person['ido']; ?>">
                <input class="ui blue fluid button" type="submit" name="submit" value="dodaj pomiar do bazy danych" />
            </div>
		</form>
	</div>
</main>

<?php } ?>