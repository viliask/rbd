<?php

require_once('db.php');

/* ------------------------------------------
------ LUDZIE
------------------------------------------ */

// wypisz informacje o użytkowniku o danym ido
function person($id) {
	global $db;

	$sql = $db->prepare("SELECT * FROM Osoby NATURAL JOIN Stanowiska WHERE ido = :id");
	$sql->bindValue(':id', (int)$id, PDO::PARAM_INT);
	$sql->execute();

    if($result = $sql->fetch())
    	return $result;
    else
    	return false;
}

// wypisz wszystkie dane wszystkich użytkowników
function persons() {
	global $db;

	$sql = $db->query('SELECT * FROM Osoby NATURAL JOIN Stanowiska ORDER BY ids ASC');

	if($result = $sql->fetchAll())
		return $result;
	else
		return false;
}

// takie tam; na później: wymyślić coś lepszego (id?)
function ifNazdorca($stanowisko) {
	if(strcasecmp($stanowisko, 'nadzorca') > 0)
		return true;
	else
		return false;
}

// wypisz stanowiska
function posts() {
	global $db;

	$sql = $db->query("SELECT * FROM Stanowiska ORDER BY ids DESC");

    if($result = $sql->fetchAll())
    	return $result;
    else
    	return false;
}

/* ------------------------------------------
------ DOŚWIADCZENIA
------------------------------------------ */

// wypisz wszystkie powierzchnie
function powierzchnie() {
	global $db;

	$sql = $db->query("SELECT * FROM Powierzchnie");

	if($result = $sql->fetchAll())
		return $result;
	else
		return false;
}

// wypisz wszystkie nawozy
function nawozy() {
	global $db;

	$sql = $db->query("SELECT * FROM Nawozy");

	if ($result = $sql->fetchAll())
		return $result;
	else
		return false;
}

// wypisz wszystkie rosliny
function rosliny() {
	global $db;

	$sql = $db->query("SELECT * FROM Rosliny");
	if ($result = $sql->fetchAll())
		return $result;
	else
		return false;
}

// pobierz aktualne prowadzone doświadczenie
function current_study() {
	global $db;

	$sql = $db->query
	(
		'SELECT * FROM Doswiadczenia WHERE start IS NOT NULL AND stop IS NULL'
	);

	if($result = $sql->fetch())
		return $result;
	else
		return false;
}

// info o doświadczeniu z danym idd
function study_info($id) {
	global $db;

	$sql = $db->prepare('SELECT * FROM Doswiadczenia WHERE idd = :id');
	$sql->bindValue(':id', (int)$id, PDO::PARAM_INT);
	$sql->execute();

	if($result = $sql->fetch())
		return $result;
	else
		return false;
}

// wypisz powierzchnie dla danego doswiadczenia
function study_pola($id) {
	global $db;

	$sql = $db->prepare
	(
		'SELECT id_pola, powierzchnia FROM Pola
		NATURAL LEFT JOIN Powierzchnie WHERE idd=:id'
	);

	$sql->bindValue(':id', (int)$id, PDO::PARAM_INT);
	$sql->execute();

	if($result = $sql->fetchAll())
		return $result;
	else
		return false;
}

// wypisz obszary dla danego pola
function study_obszary($id) {
	global $db;

	$sql = $db->prepare
	(
		'SELECT * FROM Obszary NATURAL LEFT JOIN Rosliny WHERE id_pola = :id'
	);

	$sql->bindValue(':id', (int)$id, PDO::PARAM_INT);
	$sql->execute();

	if($result = $sql->fetchAll())
		return $result;
	else
		return false;
}

// wypisz nawozy dla danego pola
function study_nawozy($id) {
	global $db;

	$sql = $db->prepare
	(
		'SELECT nawoz FROM Pola_nawozy NATURAL LEFT JOIN Nawozy WHERE id_pola = :id'
	);

	$sql->bindValue(':id', (int)$id, PDO::PARAM_INT);
	$sql->execute();

	if($result = $sql->fetchAll()) {
		return $result;
	} else {
		return false;
	}
}

function study_pomiary($id) {
	global $db;

	$sql = $db->prepare
	(
		'SELECT id_pomiaru, osoba, data, wynik, wielkosc, id_pola FROM Pomiary
		NATURAL LEFT JOIN Osoby NATURAL LEFT JOIN Obszary
		WHERE id_pola=:id'
	);

	$sql->bindValue(':id', (int)$id, PDO::PARAM_INT);
	$sql->execute();

	if($result = $sql->fetchAll()) {
		return $result;
	} else {
		return false;
	}
}

// zakończ doświadczenie
function study_end($id) {
	global $db;

	$sql = $db->prepare("UPDATE Doswiadczenia SET stop = NOW() WHERE idd = :id");
	$sql->bindValue(':id', (int)$id, PDO::PARAM_INT);
	
	if ($sql->execute())
		return true;
	else
		return false;
}

// pobierz wszystkie zakończone doświadczenia (bez pomiarów)
function fin_studies() {
	global $db;

	$sql = $db->query
	(
		"SELECT * FROM Doswiadczenia WHERE start IS NOT NULL AND stop IS NOT NULL"
	);

	if($result = $sql->fetchAll()) {
		return $result;
	} else {
		return false;
	}
}

function person_results($id) {
	global $db;

	$sql = $db->prepare
	(
		'SELECT id_pomiaru, data, wynik, wielkosc, idd, powierzchnia FROM Pomiary 
		NATURAL LEFT JOIN Obszary 
		NATURAL LEFT JOIN Pola 
		NATURAL LEFT JOIN Powierzchnie 
		NATURAL LEFT JOIN Doswiadczenia 
		WHERE ido = :id'
	);

	$sql->bindValue(":id", (int)$id, PDO::PARAM_INT);
	$sql->execute();

	if ($results = $sql->fetchAll())
		return $results;
	else
		return false;
}

/* ------------------------------------------
------ FORMULARZE - WALIDACJA
------------------------------------------ */

function test_input($data) {
	$data = trim($data);
	$data = stripslashes($data);
	$data = htmlspecialchars($data);
	return $data;
}

function alert($text, $type) {

	if (strcasecmp($type, "error") == 0) $header = "Wystąpiły błędy:";
	else if (strcasecmp($type, "success") == 0) $header = "Sukces!";
	else $header = "Informacja:";

	echo '<div class="alert '.$type.'"><h3>'.$header.'</h3>';

	if (is_array($text)) {
		foreach ($text as $tx) echo $tx . '<br>';
	} else echo $text;

	echo '</div>';

	if (strcasecmp($type, "error") == 0) {
		require_once "modules/footer.php";
		die();
	}
	
}

?>