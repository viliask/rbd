<?php

	$uname  = 'root';
	$pass   = 'maslo123';
	$dbname = 'projektSQL';
	$host   = 'localhost';

	try {
    	$db = new PDO("mysql:host=$host;dbname=$dbname", $uname, $pass);
    	$db->query('SET NAMES utf8');
		$db->query('SET CHARACTER_SET utf8_unicode_ci');
    	$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    	
    }

	catch(PDOException $e) {
    	echo "Połączenie z bazą danych nieudane: " . $e->getMessage();
    	die();
    }
